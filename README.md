Fliszar Law Office provides high-quality, aggressive criminal legal representation to clients in Allentown, Bethlehem, and Easton and the surrounding areas. We are located in Allentown, PA handling cases primarily in Northampton and Lehigh Counties.

Address: 1275 Glenlivet Dr, Suite 100, Allentown, PA 18106, USA

Phone: 484-498-4100

Website: https://fliszarlawoffice.com
